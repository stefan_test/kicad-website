+++
title = "2023 End of Year Fund Drive"
date = "2024-01-10"
draft = false
"blog/categories" = [
    "News"
]
+++

It is that time of year again!  The KiCad project is excited to announce our 2023
end of year funding drive.  With the release of KiCad version 8.0 just around the
corner, we are looking forward to the next year of development.  Our supporters'
contributions have done more to build the features of version 8 than any other
source of funding.  With your help, we can make version 9 better than ever!  Our
year end donation campaign begins this year a bit later than usual, on January 10th.

Direct funding of the KiCad project drives some of the most important features that
make KiCad the most used EDA tool in the world.  Your donations help us to pay for
the development of new features, bug fixes, and improvements to the user experience.
We are also able to use your donations to pay for the infrastructure that keeps the
project running.  This includes the website, the forum, the bug tracker, and the
continuous integration system that builds KiCad for all of the platforms that we
support.

This year, we are excited to offer an exclusive KiCad T-shirt to our monthly
sustaining donors.  This is a super-comfortable, high quality, 100% cotton t-shirt
with a unique design that is only available to our donors.  Monthly donors are
eligible to receive a shirt after they have donated a total of at least 60 USD
in 2024.

image::/img/blog/2024/sponsor_tshirt.jpg[400,400, title="Monthly Donor T-Shirt"]

If your company would like to sponsor matching donations during the 2023 funding drive,
please contact us at mailto:sponsorship@kicad.org[sponsorship@kicad.org], we have a few
dates remaining available for sponsorship this year!
