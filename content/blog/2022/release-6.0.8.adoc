+++
title = "KiCad 6.0.8 Release"
date = "2022-09-28"
draft = false
"blog/categories" = [
    "Release Notes"
]
+++

The KiCad project is proud to announce the latest series 6 stable
release.  The 6.0.8 stable version contains critical bug fixes and
other minor improvements since the previous release.

<!--more-->

A list of all of the fixed issues since the 6.0.7 release can be found
on the https://gitlab.com/groups/kicad/-/milestones/18[KiCad 6.0.8
milestone] page. This release contains several critical bug fixes so please
consider upgrading as soon as possible.

Version 6.0.8 is made from the
https://gitlab.com/kicad/code/kicad/-/commits/6.0/[6.0] branch with
some cherry picked changes from the development branch.

Packages for Windows, macOS, and Linux are available or will be
in the very near future.  See the
link:/download[KiCad download page] for guidance.

Thank you to all developers, packagers, librarians, document writers,
translators, and everyone else who helped make this release possible.

== Changelog

=== General
- https://gitlab.com/kicad/code/kicad/-/commit/c0ce4cb0bc5e8177da12293eb1aa3461bc279378[Bump vcpkg python3 to 3.9.14.]
- Fix false text on reset button on subsequent preferences dialog openings. https://gitlab.com/kicad/code/kicad/-/issues/11856[#11856].
- Hide cursor while warping on Wayland. https://gitlab.com/kicad/code/kicad/-/issues/9785[#9785]
- Remove curly brace from string encoding. https://gitlab.com/kicad/code/kicad/-/issues/12351[#12351]
- https://gitlab.com/kicad/code/kicad/-/commit/ace0032c8293f9728bbc13e7194b1c16ff1e1467[Fix build error.]
- Make enter key the OK button action in the paste special dialog. https://gitlab.com/kicad/code/kicad/-/issues/12285[#12285]
- Fix Boost version incompatibility. https://gitlab.com/kicad/code/kicad/-/issues/12175[#12175]
- Fix color picker behavior. https://gitlab.com/kicad/code/kicad/-/issues/12204[#12204]
- https://gitlab.com/kicad/code/kicad/-/commit/5f00847a86cd28144ca73b131adffa8be4135495[Fix bitmap printing in wxWidgets version greater than 3.1.5.]

=== Schematic Editor
- Fix schematic symbol library cache refresh bug. https://gitlab.com/kicad/code/kicad/-/issues/12342[#12342]
- Fix a UTF8 conversion issues in BOM python scripts. https://gitlab.com/kicad/code/kicad/-/issues/12435[#12435]
- https://gitlab.com/kicad/code/kicad/-/commit/3e8532264e6e7dc540e978b8eec7143db9763abe[Fix severe SPICE simulation memory leaks.]
- Fix crash when attempting to edit missing library symbol. https://gitlab.com/kicad/code/kicad/-/issues/11318[#11318]
- Fix inverted Y position in pin table. https://gitlab.com/kicad/code/kicad/-/issues/11988[#11988]
- https://gitlab.com/kicad/code/kicad/-/commit/26104c83bad98892f224bd2f616233eb3f6cfdff[Fix wxWidgets sizer assert in symbol preview widget.]
- Fix incorrectly displayed secondary worksheet. https://gitlab.com/kicad/code/kicad/-/issues/12017[#12017]
- Fix annotation of incomplete multi-unit and re-annotation of duplicate symbols. https://gitlab.com/kicad/code/kicad/-/issues/11496[#11496]
- Load symbols even if part definition is incomplete when importing CADSTAR schematics. https://gitlab.com/kicad/code/kicad/-/issues/11671[#11671]
- Fix broken instance data when adding new symbol. https://gitlab.com/kicad/code/kicad/-/issues/12190[#12190]
- https://gitlab.com/kicad/code/kicad/-/commit/9af666ba3459931cbbd069b43f0fcbec5abeb3be[Ensure reference field is initialized when adding a new symbol in schematic.]
- Fix missing legacy value and footprint field instance data. https://gitlab.com/kicad/code/kicad/-/issues/12226[#12226]
- Fix issue when saving labels having more than 50 characters. https://gitlab.com/kicad/code/kicad/-/issues/12151[#12151]
- Fix another case of the instance data getting messed up. https://gitlab.com/kicad/code/kicad/-/issues/11390[#11390]
- Make Eeschema net highlighting consistent with Pcbnew. https://gitlab.com/kicad/code/kicad/-/issues/11016[#11016]
- Prevent duplicate frame when importing Eagle schematics. https://gitlab.com/kicad/code/kicad/-/issues/11408[#11408]
- Handle rotation while moving from move tool. https://gitlab.com/kicad/code/kicad/-/issues/12004[#12004]
- Move sheet pins by their connection point. https://gitlab.com/kicad/code/kicad/-/issues/12134[#12134]
- Improve board file loading performance. https://gitlab.com/kicad/code/kicad/-/issues/12115[#12115]
- Fix crash attempting to add label. https://gitlab.com/kicad/code/kicad/-/issues/12094[#12094]
- Fix crash when loading CvPcb when there is a duplicate footprint library name. https://gitlab.com/kicad/code/kicad/-/issues/12080[#12080]

=== CvPcb
- Performance improvements. https://gitlab.com/kicad/code/kicad/-/issues/12063[#12063]

=== PCB Editor

- https://gitlab.com/kicad/code/kicad/-/commit/c45aa734b0ddc339c2aec9364118b3efa2458c6f[Fix missing DRC via/track check.]
- https://gitlab.com/kicad/code/kicad/-/commit/f06f205886c86519229225098e8729b7f52e6883[Fix an issue with duplicated pins in multi-unit symbols on orcadpcb2 netlist export.]
- https://gitlab.com/kicad/code/kicad/-/commit/2393165ada70a33c6766559592e72595c75a4fbb[Fix regression when loading pads in footprints with CADSTAR PCB importer.]
- https://gitlab.com/kicad/code/kicad/-/commit/e593e0b014a3952020accf6d000411c0a57fae84[Improve stop-if-walk-path-is-too-long heuristic in the P&S router walk around algorithm.]
- https://gitlab.com/kicad/code/kicad/-/commit/42dae1504a096235a6df7a0dddbacfb8e1c53075[Fix negative chamfer value for hulls generated for 0-length segments in P&S router.]
- Don't prevent immediate actions while P&S router is active. https://gitlab.com/kicad/code/kicad/-/issues/12311[#12311]
- Remove zero-sized pads on load from third party importers. https://gitlab.com/kicad/code/kicad/-/issues/12200[#12200]
- Fix crash when geometry simplification removes all outlines. https://gitlab.com/kicad/code/kicad/-/issues/12120[#12120].
- Allow board saves without modifying project files. https://gitlab.com/kicad/code/kicad/-/issues/11323[#11323].
- https://gitlab.com/kicad/code/kicad/-/commit/83f135315c03afbdddbf571a5550cca7a06ec157[Fix 3D properties dialog when a lot of 3D models are added.]
- Invert logic for guessing which layer is top / bottom in CADSTAR PCB importer. https://gitlab.com/kicad/code/kicad/-/issues/12349[#12349]
- Load 2-point polygons as line segments in CADSTAR PCB importer. https://gitlab.com/kicad/code/kicad/-/issues/12349[#12349]
- Parse teardrops in CADSTAR PCB importer. https://gitlab.com/kicad/code/kicad/-/issues/12349[#12349]
- Fix custom rule crash. https://gitlab.com/kicad/code/kicad/-/issues/12329[#12329]
- Run hole-to-hole checks on Edge_Cuts layer. https://gitlab.com/kicad/code/kicad/-/issues/12296[#12296]
- Ensure via-gap-same-as-trace-gap gets turned off for board settings. https://gitlab.com/kicad/code/kicad/-/issues/12236[#12236]
- Ensure the P&S router shove state gets reverted after routing fails. https://gitlab.com/kicad/code/kicad/-/issues/9023[#9023].
- Fix broken dynamic ratsnest line for curved traces. https://gitlab.com/kicad/code/kicad/-/issues/12205[#12205]
- Fix undo for moving group using relative position. https://gitlab.com/kicad/code/kicad/-/issues/11793[#11793]
- Fix rule less than comparison issue. https://gitlab.com/kicad/code/kicad/-/issues/12140[#12140]
- https://gitlab.com/kicad/code/kicad/-/commit/4d9802760e0c98dd958150329c79492f4287928c[Fix wxWidgets locale asserts when running Python scripts.]
- Fix Eagle plugin board layer mapping issue. https://gitlab.com/kicad/code/kicad/-/issues/11839[#11839]
- Don't close blocking dialog in back annotate. https://gitlab.com/kicad/code/kicad/-/issues/12255[#12255]
- https://gitlab.com/kicad/code/kicad/-/commit/962df45b651c104c8f17e7fff9241a88b5335b53[Fix selection tool issues.]
- Fix assertion when there are only three points in a line chain. https://gitlab.com/kicad/code/kicad/-/issues/11695[#11695]
- Allow remapping all non-electrical layers in CADSTAR PCB importer. https://gitlab.com/kicad/code/kicad/-/issues/12196[#12196]
- Report warning that imported text may be different in CADSTAR PCB importer. https://gitlab.com/kicad/code/kicad/-/issues/12195[#12195]
- https://gitlab.com/kicad/code/kicad/-/commit/36a0c2a9cc86292312c69a2f1eb534b7d0169c25[Fix issue with caches not being initialized when printing message bar.]
- Update constraint mode in status bar during move. https://gitlab.com/kicad/code/kicad/-/issues/10465[#10465]
- Fix contradictory clearance between Inspect->Clearance and DRC. https://gitlab.com/kicad/code/kicad/-/issues/11814[#11814]
- Leave originally selected items selected after cancelled move. https://gitlab.com/kicad/code/kicad/-/issues/12024[#12024]
- https://gitlab.com/kicad/code/kicad/-/commit/82e8e380546456b5227528716b0d0ab321cd3a8a[Handle via-in-pad distances when tuning with P&S router.]
- https://gitlab.com/kicad/code/kicad/-/commit/57de5a6b65ee334061109cdaf66a88111c5c112e[Handle via layers in net inspector.]
- Fix crash during drag. https://gitlab.com/kicad/code/kicad/-/issues/12137[#12137]
- Fix broken STEP export of PCB. https://gitlab.com/kicad/code/kicad/-/issues/12119[#12119]
- https://gitlab.com/kicad/code/kicad/-/commit/df9cf0a0c39ed99527b0c04e3892e8dd7ed603e7[Fix crash in P&S router walk around mode.]
- Fix unexpected behavior when changing layers while drawing dimensions. https://gitlab.com/kicad/code/kicad/-/issues/11864[#11864]
- Get rid of error prone reverse logic in global deletion dialog. https://gitlab.com/kicad/code/kicad/-/issues/12049[#12049]
- Fix distorted SVG import. https://gitlab.com/kicad/code/kicad/-/issues/12021[#12021]
- Fix through via drawing width. https://gitlab.com/kicad/code/kicad/-/issues/11851[#11851]

=== Footprint Editor
- Ensure save button is enabled correctly. https://gitlab.com/kicad/code/kicad/-/issues/11824[#11824]

=== Footprint Viewer
- Fix broken user grid selection in grid drop down list. https://gitlab.com/kicad/code/kicad/-/issues/12030[#12030]

=== 3D Viewer
- https://gitlab.com/kicad/code/kicad/-/commit/437cd8b1e56929bed0d00721c55d91637d2b1dbd[Trim down 3D viewer search paths.]
- Assign default hotkey to flip board action. https://gitlab.com/kicad/code/kicad/-/issues/12337[#12337]

=== Plugin Content Manager
- Fix path when storage location is backed by OneDrive on Windows. https://gitlab.com/kicad/code/kicad/-/issues/10165[#10165].

=== Windows
- Fix autopan sticking on Windows. https://gitlab.com/kicad/code/kicad/-/issues/11425[#11425]
